//console.log("hello world")

let num = Number(prompt("Please input a number."));
console.log("The number you provided is " + num);

for (let numCount = num; numCount > 50; numCount--){

	if (numCount % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number.")
		continue;
	}

	if (numCount % 5 === 0){
		console.log(numCount);
		continue;
	}
	
	if (numCount <= 50){
		break;
	}
		console.log("The current value is at 50. Terminating the loop.")
};

let longWord = "supercalifragilisticexpialidocious";
let consonant = ""
console.log(longWord);

for (let x = 0; x < longWord.length; x++){
	if (
	longWord[x].toLowerCase() == "a" ||
	longWord[x].toLowerCase() == "e" ||
	longWord[x].toLowerCase() == "i" ||
	longWord[x].toLowerCase() == "o" ||
	longWord[x].toLowerCase() == "u" 
		) {
		continue;
	}else{
		consonant += longWord[x];
		}	
};
console.log(consonant);
