/*
repetition control structures
	loops - executes codes repeatedly in pre-set # of times or forever
*/

//While loop - takes in an expression/condition before proceeding in the evaluation of codes
/*
SYNTAX:
while (condition)
*/



let count = 5;

while (count !== 0){
	console.log("While loop:" + count);

	count -- //for decrementing. w/o this, loop will keep counting.
};

/*
decrement: 5, 4, 3, 2, 1, 0
*/

let countTwo = 1;

while (countTwo < 11){
	console.log(countTwo);

	countTwo ++;
}

//Do While Loop - has at least 1 code block to be executed before proceeding to the condition.
/*
do{
	statement/s
}while (condition)

*/


let countThree = 5;

do{
	console.log("Do-While Loop: " + countThree);

	countThree --;
}while(countThree > 0);


let countFour = 1;

do{
	console.log(countFour);

	countFour++;
}while (countFour < 11)

//for loop - more flexible looping
/*
SYNTAX
for (initialization; condition; finalExpression){
	statement/s;
};

PROCESS
1. Initialization - recognize variable created
2. Condition - read/satisfy the condition
3. Statement/s - execute commands/statements
*/

for (let countFive = 5; countFive > 0; countFive--){
	console.log("For Loop: "+ countFive)
}

let number = Number(prompt("Give me a number"));
for (let numCount = 1; numCount <= number; numCount++){
	console.log("Hello Batch 170!")
}

let myName = "alex";
/*

.length - count the # of characters in string including spaces, symbols, punctuations
console.log(myName.length); 

variable[x] - accessing the array/string of variable; array is 0-based, will start count at 0.
console.log(myName[2]); //accessing an array
*/

for (let x = 0; x < myName.length; x++){
	console.log(myName[x])
};

myName = "Alex";
for (let i = 0; i < myName.length; i++){
	if (
	myName[i].toLowerCase() == "a" ||
	myName[i].toLowerCase() == "e" ||
	myName[i].toLowerCase() == "i" ||
	myName[i].toLowerCase() == "o" ||
	myName[i].toLowerCase() == "u" 
		) {
		console.log(3);
	}else{
		console.log(myName[i]);
	}
}

//Continue & Break Statements

for (let countSix = 0; countSix <= 20; countSix++){
	if (countSix % 2 === 0){
		continue; //tells browser/codes to continue to next iteration of loop
	}
	console.log("Continue and Break: " + countSix);
	
	if (countSix > 10){
		break; //tells browser/code to terminate loop even if condition is met/satisfied
	}
}

let name = "Alexandro";
//name has 8 indeces (name[index]) but 9 elements(.length)
for (let i = 0; i < name.length; i++){
	console.log(name[i]);

	if (name[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration");
		continue;
	}
	if (name[i].toLowerCase() === "d"){
		break
	}
}